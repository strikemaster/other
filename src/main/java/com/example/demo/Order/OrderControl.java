package com.example.demo.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class OrderControl {

    @Autowired
    OrderService orderService;

    @PostMapping("order")
    public void saveOrder(@RequestBody Order order){


        orderService.saveOrder(order);


    }
}
