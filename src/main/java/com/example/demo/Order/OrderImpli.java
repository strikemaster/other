package com.example.demo.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class OrderImpli implements OrderService {

    @Autowired
    OrderDb orderDb;

    @Override
    public void saveOrder(Order order) {

        System.out.println(order);

        orderDb.saveAndFlush(order);

    }
}
