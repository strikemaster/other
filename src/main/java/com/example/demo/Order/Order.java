package com.example.demo.Order;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Component
@Data
@Table(name = "order_placement")
public class Order {

    @Id
    @Column(name = "order_number")
    int orderNumber;

    @Column(name = "product_name")
    String productName;

    @Column(name = "quantity")
    int quantity;

    @Column(name = "supplier")
    String supplier;
}
