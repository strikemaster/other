package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class StockControl {

    @Autowired
    StockImpli stockImpli;

    @GetMapping("stock")
    public List<Stock>getStock(){

       return stockImpli.getStock();

    }

    @PostMapping("saveStock")
    public void saveStock(@RequestBody Stock stock){


        System.out.println(stock);

        stockImpli.saveStock(stock);


    }


}
