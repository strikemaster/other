package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@Table(name = "stock")
public class Stock {

    @Id
    @Column(name="bar_code")
    int barcode;

    @Column(name = "product_name")
    String productName;

    @Column(name="price")
    int price;

    @Column(name = "quantity")
    int quantity;



}
