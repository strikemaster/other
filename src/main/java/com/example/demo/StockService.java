package com.example.demo;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface StockService {

    List<Stock> getStock();

    void saveStock(Stock stock);


}
