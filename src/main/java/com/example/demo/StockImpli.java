package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockImpli implements StockService {

    @Autowired
    StockDb stockDb;

    @Override
    public List<Stock> getStock() {

        System.out.println(stockDb.findAll());

        return stockDb.findAll();
    }

    @Override
    public void saveStock(Stock stock) {


        stockDb.saveAndFlush(stock);

    }
}
